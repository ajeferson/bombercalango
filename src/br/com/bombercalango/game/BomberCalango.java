package br.com.bombercalango.game;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import br.com.bombercalango.graphic.BaseVectorShape;
import br.com.bombercalango.model.Bomb;
import br.com.bombercalango.model.Calango;
import br.com.bombercalango.model.Explosion;
import br.com.bombercalango.model.Rock;
import br.com.bombercalango.model.enemy.Enemy;
import br.com.bombercalango.model.level.Level;
import br.com.bombercalango.model.level.Level1;
import br.com.bombercalango.model.level.Level2;
import br.com.bombercalango.model.level.Level3;
import br.com.bombercalango.other.ExplosionRange;
import br.com.bombercalango.other.Status;

/**
 * Class in which the game starts.
 * @author Alan Jeferson
 * @version 1.0
 * */
public class BomberCalango extends Applet implements Runnable, KeyListener{

	private static final long serialVersionUID = 923210300488096839L;

	/* Config Attributes */
	private static final int GAME_WIDTH = 660;
	private static final int GAME_HEIGHT = 720;

	/* Core elements */
	private AffineTransform transform;
	private Graphics2D graphics;
	private BufferedImage bufferedImage;
	private Thread thread;

	/* Game elements */
	private Calango player;
	private List<Level> levels;
	private Level currentLevel;
	private Rock[][] rockMatrix;
	private BaseVectorShape[][] stuffMatrix;
	private Bomb bomb;
	private Explosion explosion;
	private Status status;
	
	/* Sounds */
	private AudioClip bombEffect;
	private AudioClip levelFinishedEffect;
	private AudioClip backgroundSound;
	private AudioClip walkEffect;
	private AudioClip gameOverEffect;
	private AudioClip winEffect;
	private AudioClip ouchEffect;

	/**
	 * Making first configurations
	 * */
	@Override
	public void init() {
		this.transform = new AffineTransform();
		this.transform.translate(0, GAME_HEIGHT-GAME_WIDTH);
		this.bufferedImage = new BufferedImage(GAME_WIDTH, GAME_HEIGHT, BufferedImage.TYPE_INT_RGB);
		this.graphics = this.bufferedImage.createGraphics();
		this.setSize(GAME_WIDTH, GAME_HEIGHT);
		
		/* sounds */
		this.bombEffect = getAudioClip(getCodeBase(), "bomb.wav");		
		this.levelFinishedEffect = getAudioClip(getCodeBase(), "level_finished.wav");
		this.walkEffect = getAudioClip(getCodeBase(), "walk.wav");
		this.gameOverEffect = getAudioClip(getCodeBase(), "game_over.wav");
		this.winEffect = getAudioClip(getCodeBase(), "winner.wav");
		this.ouchEffect = getAudioClip(getCodeBase(), "ouch.wav");

		/* Setting the title */
		Frame c = (Frame)this.getParent().getParent();
		c.setTitle("BomberCalango Version 1.0");
		c.setResizable(false);

		/* Creating levels */
		this.levels = new ArrayList<Level>();
		this.createLevels();
		this.currentLevel = this.levels.get(0);
		
		this.playBackgroundSound();
		
		/* Creating game elements */
		this.player = new Calango();
		this.rockMatrix = new Rock[11][11];
		this.stuffMatrix = new BaseVectorShape[11][11];

		this.createRocks();
		this.createStuff();
		
		this.status = Status.PLAYING;

		this.addKeyListener(this);
	}
	
	/**
	 * Creates the levels;
	 * */
	private void createLevels(){
		this.levels.clear();
		this.levels.add(new Level1());
		this.levels.add(new Level2());
		this.levels.add(new Level3());
	}
	
	/**
	 * Loops the background sound.
	 * */
	private void playBackgroundSound(){
		if(this.backgroundSound!=null){
			this.backgroundSound.stop();
		}
		this.backgroundSound = getAudioClip(getCodeBase(), this.currentLevel.getSound());
		this.backgroundSound.loop();
	}

	/**
	 *  Creating Rocks 
	 *  */
	private void createRocks(){
		for(int i=0;i<this.currentLevel.getRockMatrix().length;i++){
			for(int g=0;g<this.currentLevel.getRockMatrix()[i].length;g++){
				if(this.currentLevel.getRockMatrix()[i][g]){
					this.rockMatrix[i][g] = new Rock(g*60, i*60);
				}
			}
		}
	}
	
	@SuppressWarnings("rawtypes")
	private void createStuff(){
		/* Creating Cactus */
		Constructor constructor = null;
		try {
			constructor = this.currentLevel.getType().getConstructor(Integer.class, Integer.class);
		} catch (NoSuchMethodException e1) {
			e1.printStackTrace();
		} catch (SecurityException e1) {
			e1.printStackTrace();
		}
		for(int i=0;i<this.currentLevel.getStuffMatrix().length;i++){
			for(int g=0;g<this.currentLevel.getStuffMatrix()[i].length;g++){
				if(this.currentLevel.getStuffMatrix()[i][g]){
					try {
						this.stuffMatrix[i][g] = (BaseVectorShape) constructor.newInstance(new Integer(g*60), new Integer(i*60));
					} catch (InstantiationException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	/**
	 * Starting the game
	 * */  
	@Override
	public void start() {
		this.thread = new Thread(this);
		this.thread.start();
	}

	/**
	 * Redrawing game elements
	 * */
	@Override
	public void update(Graphics g) {
		this.requestFocus();
		this.graphics.setTransform(this.transform);
		this.graphics.setPaint(this.currentLevel.getBackgroundColor());
		this.graphics.fillRect(0, 0, getSize().width, getSize().height);

		/* Drawing */
		this.drawScenario(this.stuffMatrix);
		if(this.bomb!=null){
			if(this.bomb.getLifetime()>0){
				this.drawBomb();
			} else{
				this.explosion = new Explosion(this.bomb, this.currentLevel.getRockMatrix());
				this.bombEffect.play();
				
				/* Destroying cactus */
				for(int i=this.explosion.getxBegin()>=0 ? 
						this.explosion.getxBegin() : 0; 
						i<this.explosion.getxEnd() && i<11;i++){
					if(this.currentLevel.getStuffMatrix()[bomb.getCoordY()][i]){
						this.currentLevel.getStuffMatrix()[bomb.getCoordY()][i] = false;
						this.stuffMatrix[bomb.getCoordY()][i] = null;
					}
				}
				
				for(int i=this.explosion.getyBegin()>=0 ? 
						this.explosion.getyBegin() : 0; 
						i<this.explosion.getyEnd() && i<11;i++){
					if(this.currentLevel.getStuffMatrix()[i][bomb.getCoordX()]){
						this.currentLevel.getStuffMatrix()[i][bomb.getCoordX()] = false;
						this.stuffMatrix[i][bomb.getCoordX()] = null;
					}
				}
				
				this.bomb = null;
			}
		}		
		
		if(this.explosion!=null){
			if(this.explosion.getLifetime()>0){
				this.drawExplosion();
				this.explosion.decreaseLifetime();
				
				ExplosionRange range = this.explosion.getExplosionRange();
				
				/* Check colision with enemies */
				for(int i=0;i<this.currentLevel.getEnemies().size();i++){
					if(range.isInExplosionRange(this.currentLevel.getEnemies().get(i).getCoordX(), 
							this.currentLevel.getEnemies().get(i).getCoordY())){
						this.currentLevel.getEnemies().remove(i);
						this.player.incrementKills();
					}
				}
				
				/* Checking explosion colision with player */
				if(range.isInExplosionRange(this.player.getCoordY(), this.player.getCoordX()) 
						&& !player.isDead()){
					this.player.die();
					this.ouchEffect.play();
					if(this.player.getLives()>0){
						this.player.setCoordX(0);
						this.player.setCoordY(0);
						this.player.setFaceAngle(90);
					}
				}
				
			} else{
				this.explosion = null;
				if(this.player.getLives()>0) this.player.setDead(false);
			}
		}
		
		if(!this.player.isDead()){
			this.drawPlayer();
		}
		this.drawScenario(this.rockMatrix);
		
		this.graphics.setTransform(new AffineTransform());
		this.graphics.setColor(Color.BLACK);
		this.graphics.fillRect(0, 0, 660, 60);
		
		if(!this.currentLevel.getEnemies().isEmpty()){
			this.drawEnemies();
			
			this.graphics.setTransform(new AffineTransform());
			this.graphics.setColor(Color.WHITE);
			this.graphics.scale(2, 2);
			
			if(this.player.getLives()>0){
				this.graphics.drawString("Lives: " + this.player.getLives(), 5, 20);
				this.graphics.drawString("Kills: " + this.player.getKills(), 60, 20);
			} else{
				if(!this.status.equals(Status.GAME_OVER)){
					this.gameOverEffect.play();
				}
				this.status = Status.GAME_OVER;
				this.graphics.drawString("Game Over. Press ENTER to restart.", 5, 20);
				this.player.setCoordX(100);
				this.player.setCoordY(100);
			}
			
		} else{
			if(this.status.equals(Status.PLAYING)){
				this.levelFinishedEffect.play();
			}
			this.graphics.setColor(Color.WHITE);
			this.graphics.scale(2, 2);
			if(this.levels.size()>1){
				this.graphics.drawString("Level finished. Press ENTER to move on.", 5, 20);
				this.status = Status.LEVEL_FINISHED;
			} else{
				this.graphics.drawString("You've saved Ana! Now you can marry her! :)  Kills: " + 
							this.player.getKills(), 5, 20);
				if(!this.status.equals(Status.GAME_FINISHED)){
					this.winEffect.play();
				}
				this.status = Status.GAME_FINISHED;
			}
		}
		
		this.paint(g);
	}

	@Override
	public void paint(Graphics g) {
		g.drawImage(this.bufferedImage, 0, 0, this);
	}

	/**
	 * Game loop
	 * */
	@Override
	public void run() {
		Thread t = Thread.currentThread();
		while(t == this.thread){
			try {
				repaint();
				Thread.sleep(0);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Draws the players.
	 * */
	private void drawPlayer(){
		int offsetX = 0, offsetY = 0;
		switch (this.player.getFaceAngle()) {
		case 0:
			offsetX = 12;
			break;
		case 180:
			offsetX = 50;
			offsetY = 60;
			break;
		case 90:
			offsetX = 60;
			offsetY = 12;
			break;
		case 270:
			offsetX = 0;
			offsetY = 47;
			break;
		}
		this.graphics.setTransform(this.transform);
		this.graphics.translate(this.player.getCoordX()*60+offsetX, this.player.getCoordY()*60+offsetY);
		this.graphics.rotate(Math.toRadians(this.player.getFaceAngle()));
		this.graphics.setColor(Color.WHITE);
		this.graphics.scale(3, 3);
		this.graphics.fill(this.player.getShape());
	}

	/**
	 * Draw static things of scenario
	 * */
	private void drawScenario(BaseVectorShape[][] matrix){
		for(int i=0;i<matrix.length;i++){
			for(int g=0;g<matrix[i].length;g++){
				BaseVectorShape shape = matrix[i][g];
				if(shape!=null){
					this.graphics.setTransform(this.transform);
					this.graphics.setColor(shape.getColor());
					this.graphics.translate(shape.getX(), shape.getY());
					this.graphics.scale(3, 3);
					this.graphics.fill(shape.getShape());
				}
			}
		}
	}	

	/**
	 * Draws the bomb in the game.
	 * */
	private void drawBomb(){
		this.graphics.setTransform(this.transform);
		this.graphics.translate(this.bomb.getCoordX()*60, this.bomb.getCoordY()*60);
		this.graphics.setColor(Color.BLACK);
		this.graphics.scale(3,  3);
		if(this.bomb.getLifeTime()==0){
			this.bomb.restoreAnimationTime();
			if(this.bomb.isIncreasing()){
				if(this.bomb.getRadius()<20){
					this.bomb.increaseRadius();
				} else{
					this.bomb.setIncreasing(false);
					this.bomb.decreaseRadius();
				}
			} else{
				if(this.bomb.getRadius()>17){
					this.bomb.decreaseRadius();
				} else{
					this.bomb.setIncreasing(true);
					this.bomb.increaseRadius();
				}
			}
		} else{
			this.bomb.decreaseAnimationTime();
		}
		this.graphics.fillOval((20-this.bomb.getRadius())/2, (20-this.bomb.getRadius())/2, this.bomb.getRadius(), this.bomb.getRadius());
	}
	
	/**
	 * Draws an explosion when a bomb lifetime is over.
	 * */
	private void drawExplosion(){
		this.graphics.setTransform(this.transform);
		this.graphics.setColor(Color.RED);
		this.graphics.fillRoundRect((this.explosion.getxBegin()*60)+5, 
								this.explosion.getYCenter()*60+5, 
								(this.explosion.getxEnd()*60-10 - this.explosion.getxBegin()*60), 
								50, 80, 80);
		this.graphics.fillRoundRect(this.explosion.getXCenter()*60+5, 
									this.explosion.getyBegin()*60, 50, 
									((this.explosion.getyEnd()*60-10)-(this.explosion.getyBegin()*60)), 
									80, 80);
	}
	
	
	/**
	 * Draws all the enemies.
	 * */
	private void drawEnemies(){
		for(Enemy enemy : this.currentLevel.getEnemies()){
			if(enemy.getAnimationTime()==0){
				enemy.move(this.currentLevel.getRockMatrix(), this.currentLevel.getStuffMatrix(), this.bomb, this.explosion);
				enemy.restoreAnimationTime();
				if(enemy.getCoordX()==this.player.getCoordY() && 
						enemy.getCoordY()==this.player.getCoordX()){
					this.player.die();
					this.ouchEffect.play();
					if(this.player.getLives()>0){
						this.player.setDead(false);
					}
				}
			} else{
				enemy.decreaseAnimationTime();
			}
			this.graphics.setTransform(this.transform);
			this.graphics.translate(enemy.getTranslationY(), enemy.getTranslationX());
			enemy.drawEnemy(this.graphics);
		}
	}
	
	/**
	 * Key Event to move player
	 * */
	@Override
	public void keyPressed(KeyEvent event) {
		final int PLAYER_MOV_SPPED = 60;
		int coordX = this.player.getCoordX();
		int coordY = this.player.getCoordY();
		
		switch (event.getKeyCode()) {
		case KeyEvent.VK_W:	
		case KeyEvent.VK_UP:
			if(coordY>0 && 
				!this.currentLevel.getRockMatrix()[coordY-1][coordX] && 
					!this.currentLevel.getStuffMatrix()[coordY-1][coordX] && 
						(this.bomb==null || (coordY-1!=this.bomb.getCoordY() || 
							coordX!=this.bomb.getCoordX()))){
				this.player.decY(PLAYER_MOV_SPPED);
				this.player.setCoordY(coordY-1);
				this.walkEffect.play();
			}
			if(this.player.getFaceAngle()!=0){
				this.player.setFaceAngle(0);
			}
			break;
		case KeyEvent.VK_S:	
		case KeyEvent.VK_DOWN:
			if(coordY+1<=10 &&
				!this.currentLevel.getRockMatrix()[coordY+1][coordX] &&
					!this.currentLevel.getStuffMatrix()[coordY+1][coordX] &&
						(this.bomb==null || (coordY+1!=this.bomb.getCoordY() || 
							coordX!=this.bomb.getCoordX()))){
				this.player.incY(PLAYER_MOV_SPPED);
				this.player.setCoordY(coordY+1);
				this.walkEffect.play();
			}
			if(this.player.getFaceAngle()!=180){
				this.player.setFaceAngle(180);
			}
			break;
		case KeyEvent.VK_A:	
		case KeyEvent.VK_LEFT:
			if(coordX>0 &&
				!this.currentLevel.getRockMatrix()[coordY][coordX-1] &&
					!this.currentLevel.getStuffMatrix()[coordY][coordX-1] &&
						(this.bomb==null || (coordX-1!=this.bomb.getCoordX() || 
							coordY!=this.bomb.getCoordY()))){
				this.player.decX(PLAYER_MOV_SPPED);
				this.player.setCoordX(coordX-1);
				this.walkEffect.play();
			}
			if(this.player.getFaceAngle()!=270){
				this.player.setFaceAngle(270);
			}
			break;
		case KeyEvent.VK_D:	
		case KeyEvent.VK_RIGHT:
			if(coordX+1<=10 &&
				!this.currentLevel.getRockMatrix()[coordY][coordX+1] &&
					!this.currentLevel.getStuffMatrix()[coordY][coordX+1] &&
						(this.bomb==null || (coordX+1!=this.bomb.getCoordX() || 
							coordY!=this.bomb.getCoordY()))){
				this.player.incX(PLAYER_MOV_SPPED);
				this.player.setCoordX(coordX+1);
				this.walkEffect.play();
			} 
			if(this.player.getFaceAngle()!=90){
				this.player.setFaceAngle(90);
			}
			break;
		case 32:
			if(this.bomb==null){
				this.bomb = new Bomb(this.player.getCoordX(), this.player.getCoordY(), 
						20, this.currentLevel.getRockMatrix());
			}
			break;
		case 10:
			if(this.status.equals(Status.LEVEL_FINISHED)){
				this.explosion = null;
				this.bomb = null;
				this.levels.remove(0);
				if(!this.levels.isEmpty()){
					this.currentLevel = this.levels.get(0);
					this.stuffMatrix = new BaseVectorShape[11][11];
					this.createStuff();
					this.player.restorePosition();
					this.status = Status.PLAYING;
					this.backgroundSound.stop();
					this.playBackgroundSound();
				}
			} else if(this.status.equals(Status.GAME_OVER)){
				this.createLevels();
				this.currentLevel = this.levels.get(0);
				this.stuffMatrix = new BaseVectorShape[11][11];
				this.createStuff();
				this.player.setLives(Calango.DEFAULT_LIVES);
				this.player.restorePosition();
				this.status = Status.PLAYING;
				this.backgroundSound.stop();
				this.playBackgroundSound();
				this.player.setDead(false);
				this.player.restoreKills();
			}
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
	}

	@Override
	public void keyTyped(KeyEvent arg0) {		
	}

}
