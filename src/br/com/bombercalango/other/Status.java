package br.com.bombercalango.other;

/**
 * Describes the different status
 * of the game.
 * */
public enum Status {
	
	PLAYING,
	LEVEL_FINISHED,
	GAME_OVER,
	GAME_FINISHED

}
