package br.com.bombercalango.other;

import br.com.bombercalango.model.Explosion;

/**
 * Represents the explosion range of a bomb.
 * @author Alan Jeferson
 * */
public class ExplosionRange {

	private int[] xCoords, yCoords;
	private int originX, originY;
	
	public ExplosionRange(int coordX, int coordY, boolean[][] rockMatrix){
		this.originX = coordX;
		this.originY = coordY;
		
		/* Getting X coordenates */
		int xBegin = coordX-Explosion.EXPLOSION_POWER;
		int xEnd = coordX+Explosion.EXPLOSION_POWER;
		for(int i=coordX;i>=xBegin && i>=0;i--){ //Left limit
			if(rockMatrix[coordY][i]){
				xBegin = i+1;
				break;
			}
		}
		for(int i=coordX>0 ? coordX : 0;
				i<=xEnd && i<11;i++){ //Right limit
			if(rockMatrix[coordY][i]){
				xEnd = i;
				break;
			}
		}

		/* Getting Y coordenates */
		int yBegin = coordY-Explosion.EXPLOSION_POWER;
		int yEnd = coordY+Explosion.EXPLOSION_POWER;
		for(int i=coordY;i<yEnd && i<11;i++){ //Inferior limit
			if(rockMatrix[i][coordX]){
				yEnd = i;
				break;
			}
		}
		for(int i=coordY;i>=yBegin && i>=0;i--){ //Superior Limit
			if(rockMatrix[i][coordX]){
				yBegin = i+2;
				break;
			}
		}
		
		this.xCoords = new int[(yEnd-yBegin)+(xEnd-xBegin)+2];
		this.yCoords = new int[(yEnd-yBegin)+(xEnd-xBegin)+2];
		
		int index = 0;
		for(int i=xBegin;i<=xEnd;i++){
			this.xCoords[index] = coordY;
			this.yCoords[index++] =  i;
		}
		
		for(int i=yBegin;i<=yEnd;i++){
			this.xCoords[index] = i;
			this.yCoords[index++] = coordX;
		}
	}
	
	public boolean isInExplosionRange(int coordX, int coordY){
		for(int i=0;i<this.xCoords.length;i++){
			if(coordX==this.xCoords[i] && coordY==this.yCoords[i]){
				return true;
			}
		}
		return false;
	}

	public int[] getxCoords() {
		return xCoords;
	}

	public void setxCoords(int[] xCoords) {
		this.xCoords = xCoords;
	}

	public int[] getyCoords() {
		return yCoords;
	}

	public void setyCoords(int[] yCoords) {
		this.yCoords = yCoords;
	}

	public int getOriginY() {
		return originY;
	}

	public void setOriginY(int originY) {
		this.originY = originY;
	}

	public int getOriginX() {
		return originX;
	}

	public void setOriginX(int originX) {
		this.originX = originX;
	}
	
}
