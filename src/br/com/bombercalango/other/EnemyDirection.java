package br.com.bombercalango.other;

/**
 * Describes in which direction
 * an enemy are moving.
 * @author Alan Jeferson
 * */
public enum EnemyDirection {
	UPWARD,
	DOWNWARD,
	LEFTWARD,
	RIGHTWARD,
	NONE
}
