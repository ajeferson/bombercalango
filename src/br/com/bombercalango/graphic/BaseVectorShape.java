package br.com.bombercalango.graphic;
/*****************************************************
 * Beginning Java Game Programming, 2nd Edition
 * by Jonathan S. Harbour
 * Base vector shape class for polygonal shapes
 *****************************************************/

import java.awt.Color;
import java.awt.Shape;

public class BaseVectorShape {
	
    private Shape shape;
    private double x,y;
    private Color color;
    private int faceAngle;
    
    public BaseVectorShape() {
        setShape(null);
        setX(0.0);
        setY(0.0);
    }

    /* Getters */
    public Shape getShape() { return shape; }
    public double getX() { return x; }
    public double getY() { return y; }

    /* Setters */
    public void setShape(Shape shape) { this.shape = shape; }
    public void setX(double x) { this.x = x; }
    public void incX(double i) { this.x += i; }
    public void decX(double i) { this.x -= i; }
    public void setY(double y) { this.y = y; }
    public void incY(double i) { this.y += i; }
    public void decY(double i) { this.y -= i; }
    
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}

	public int getFaceAngle() {
		return faceAngle;
	}

	public void setFaceAngle(int faceAngle) {
		this.faceAngle = faceAngle;
	}
}
