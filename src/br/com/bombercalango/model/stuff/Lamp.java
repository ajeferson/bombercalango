package br.com.bombercalango.model.stuff;

import java.awt.Color;
import java.awt.Polygon;

import br.com.bombercalango.graphic.BaseVectorShape;

/**
 * A lamp stuff.
 * @author Alan Jeferson
 * */
public class Lamp extends BaseVectorShape{
	
	private int[] xCoords = {2, 18, 14, 18, 2, 6, 2};
	private int[] yCoords = {2, 2, 10, 18, 18, 10, 2};
	
	public Lamp(Integer x, Integer y){
		this.setShape(new Polygon(this.xCoords, this.yCoords, this.xCoords.length));
		this.setColor(new Color(255, 201, 14));
		this.setX(x.intValue());
		this.setY(y.intValue());
	}

}
