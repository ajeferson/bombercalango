package br.com.bombercalango.model.stuff;

import java.awt.Color;
import java.awt.Polygon;

import br.com.bombercalango.graphic.BaseVectorShape;

/**
 * Abstract a cactus game object.
 * @author Alan Jeferson
 * */
public class Cactus extends BaseVectorShape{
	
	private int[] xCoords = {8, 8, 2, 2, 6, 6, 8, 8, 12, 12, 14, 14, 18, 18, 12, 12, 8};
	private int[] yCoords = {18, 14, 14, 6, 6, 10, 10, 2, 2, 10, 10, 4, 4, 14, 14, 18, 18};
	
	public Cactus(Integer x, Integer y){
		this.setShape(new Polygon(this.xCoords, this.yCoords, this.xCoords.length));
		this.setColor(Color.GREEN);
		this.setX(x.intValue());
		this.setY(y.intValue());
	}

}
