package br.com.bombercalango.model.stuff;

import java.awt.Color;
import java.awt.Polygon;

import br.com.bombercalango.graphic.BaseVectorShape;

public class Losangle extends BaseVectorShape{
	
	private int[] xCoords = {9, 11,    18, 18,   11, 9,     2, 2,    9};
	private int[] yCoords = {2, 2,    9, 11,     18, 18,    11, 9,    2};
	
	public Losangle(Integer x, Integer y){
		this.setShape(new Polygon(this.xCoords, this.yCoords, this.xCoords.length));
		this.setColor(Color.CYAN);
		this.setX(x.intValue());
		this.setY(y.intValue());
	}

}
