package br.com.bombercalango.model.level;

import java.awt.Color;
import java.util.ArrayList;

import br.com.bombercalango.model.enemy.Enemy;
import br.com.bombercalango.model.enemy.Pizza;
import br.com.bombercalango.model.stuff.Losangle;

public class Level3 extends Level{
	
	public Level3(){
		
		this.type = Losangle.class;
		
		this.backgroundColor = new Color(128, 0, 255);
		
		this.sound = "level_3.wav";
		
		this.setRockMatrix(new boolean[][]{
			{false, false, false, false, false, false, false, false, false, false, false},
			{false, true, false, true, false, true, false, true, false, true, false},
			{false, false, false, false, false, false, false, false, false, false, false},
			{false, true, false, true, false, true, false, true, false, true, false},
			{false, false, false, false, false, false, false, false, false, false, false},
			{false, true, false, true, false, true, false, true, false, true, false},
			{false, false, false, false, false, false, false, false, false, false, false},
			{false, true, false, true, false, true, false, true, false, true, false},
			{false, false, false, false, false, false, false, false, false, false, false},
			{false, true, false, true, false, true, false, true, false, true, false},
			{false, false, false, false, false, false, false, false, false, false, false}});
		
		this.setStuffMatrix(new boolean[][]{
				{false, false, true, true, true, true, true, true, true, false, false},
				{false, false, false, false, false, false, false, false, true, false, false},
				{true, true, false, false, false, true, true, true, true, true, true},
				{true, false, true, false, true, false, false, false, true, false, true},
				{true, true, false, true, true, true, true, true, false, false, false},
				{true, false, true, false, true, false, false, false, true, false, true},
				{false, true, false, true, true, true, true, false, true, true, true},
				{false, false, true, false, true, false, true, false, true, false, true},
				{true, false, false, true, false, true, true, true, true, true, false},
				{true, false, true, false, false, false, true, false, true, false, false},
				{true, true, true, false, false, false, true, true, true, true, false}});
		
		this.enemies = new ArrayList<Enemy>();
		this.enemies.add(new Pizza(4, 2));
		this.enemies.add(new Pizza(3, 6));
		this.enemies.add(new Pizza(1, 6));
		this.enemies.add(new Pizza(5, 6));
		this.enemies.add(new Pizza(1, 2));
		this.enemies.add(new Pizza(1, 4));
		this.enemies.add(new Pizza(0, 10));
		this.enemies.add(new Pizza(7, 0));
		this.enemies.add(new Pizza(4, 10));
		this.enemies.add(new Pizza(8, 1));
		this.enemies.add(new Pizza(10, 3));
		this.enemies.add(new Pizza(9, 4));
		this.enemies.add(new Pizza(10, 5));
		this.enemies.add(new Pizza(10, 10));
		this.enemies.add(new Pizza(6, 7));
		
	}

}
