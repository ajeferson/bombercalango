package br.com.bombercalango.model.level;

import java.awt.Color;
import java.util.ArrayList;

import br.com.bombercalango.model.enemy.Enemy;
import br.com.bombercalango.model.enemy.Coke;
import br.com.bombercalango.model.stuff.Lamp;

public class Level2 extends Level{

	public Level2() {
		
		this.type = Lamp.class;
		
		this.backgroundColor = new Color(0, 0, 64);
		
		this.sound = "level_2.wav";
		
		this.setRockMatrix(new boolean[][]{
			{false, false, false, false, false, false, false, false, false, false, false},
			{false, true, false, true, false, true, false, true, false, true, false},
			{false, false, false, false, false, false, false, false, false, false, false},
			{false, true, false, true, false, true, false, true, false, true, false},
			{false, false, false, false, false, false, false, false, false, false, false},
			{false, true, false, true, false, true, false, true, false, true, false},
			{false, false, false, false, false, false, false, false, false, false, false},
			{false, true, false, true, false, true, false, true, false, true, false},
			{false, false, false, false, false, false, false, false, false, false, false},
			{false, true, false, true, false, true, false, true, false, true, false},
			{false, false, false, false, false, false, false, false, false, false, false}});
		
		this.setStuffMatrix(new boolean[][]{
				{false, false, true, false, false, true, true, true, true, true, true},
				{false, false, true, false, true, false, true, false, true, false, true},
				{true, false, true, true, true, true, true, true, true, true, false},
				{true, false, true, false, true, false, false, false, true, false, false},
				{true, true, false, true, true, true, true, true, true, false, false},
				{true, false, true, false, false, false, false, false, true, false, true},
				{true, true, false, true, false, true, true, false, true, true, true},
				{true, false, true, false, true, false, true, false, true, false, true},
				{true, true, true, true, true, true, true, true, false, false, false},
				{true, false, true, false, true, false, true, false, true, false, true},
				{true, true, true, false, false, true, true, true, true, true, true}});
		
		this.enemies = new ArrayList<Enemy>();
		this.enemies.add(new Coke(10, 3));
		this.enemies.add(new Coke(2, 10));
		this.enemies.add(new Coke(2, 1));
		this.enemies.add(new Coke(0, 4));
		this.enemies.add(new Coke(5, 4));
		this.enemies.add(new Coke(8, 10));
		this.enemies.add(new Coke(4, 9));
	}

}
