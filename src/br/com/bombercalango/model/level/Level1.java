package br.com.bombercalango.model.level;

import java.awt.Color;
import java.util.ArrayList;

import br.com.bombercalango.model.enemy.Enemy;
import br.com.bombercalango.model.enemy.Hamburger;
import br.com.bombercalango.model.stuff.Cactus;

/**
 * Abstracts a level of the game.
 * @author Alan Jeferson
 * */
public class Level1 extends Level{
	
	public Level1(){
		
		this.type = Cactus.class;
		
		this.backgroundColor = new Color(111, 46, 6);
		
		this.sound = "level_1.wav";
		
		this.setRockMatrix(new boolean[][]{
			{false, false, false, false, false, false, false, false, false, false, false},
			{false, true, false, true, false, true, false, true, false, true, false},
			{false, false, false, false, false, false, false, false, false, false, false},
			{false, true, false, true, false, true, false, true, false, true, false},
			{false, false, false, false, false, false, false, false, false, false, false},
			{false, true, false, true, false, true, false, true, false, true, false},
			{false, false, false, false, false, false, false, false, false, false, false},
			{false, true, false, true, false, true, false, true, false, true, false},
			{false, false, false, false, false, false, false, false, false, false, false},
			{false, true, false, true, false, true, false, true, false, true, false},
			{false, false, false, false, false, false, false, false, false, false, false}});
		
		this.setStuffMatrix(new boolean[][]{
				{false, false, true, true, true, true, true, true, true, true, true},
				{false, false, true, false, true, false, true, false, true, false, true},
				{true, true, true, true, true, true, true, true, false, false, false},
				{true, false, true, false, true, false, false, false, true, false, false},
				{true, true, true, false, false, false, true, true, true, true, true},
				{true, false, true, false, true, false, false, false, true, false, true},
				{true, true, false, true, true, true, true, false, false, false, true},
				{true, false, true, false, true, false, true, false, true, false, true},
				{true, false, false, true, true, true, true, true, false, false, true},
				{true, false, true, false, true, false, true, false, false, false, true},
				{true, true, true, false, true, true, false, true, false, false, true}});
		
		this.enemies = new ArrayList<Enemy>();
		this.enemies.add(new Hamburger(4, 3));
		this.enemies.add(new Hamburger(10, 9));
		this.enemies.add(new Hamburger(3, 10));
		this.enemies.add(new Hamburger(8, 2));
		this.enemies.add(new Hamburger(6, 7));
	}
	
}
