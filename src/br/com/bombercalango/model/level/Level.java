package br.com.bombercalango.model.level;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import br.com.bombercalango.graphic.BaseVectorShape;
import br.com.bombercalango.model.enemy.Enemy;

public abstract class Level {
	
	protected Class<? extends BaseVectorShape> type;
	
	protected boolean[][] rockMatrix;
	
	protected boolean[][] stuffMatrix;
	
	protected List<Enemy> enemies;
	
	protected boolean finished;
	
	protected Color backgroundColor;
	
	protected String sound;

	public Level(){
		this.finished = false;
		this.enemies = new ArrayList<Enemy>();
	}
	
	public boolean[][] getRockMatrix() {
		return rockMatrix;
	}

	public void setRockMatrix(boolean[][] rockMatrix) {
		this.rockMatrix = rockMatrix;
	}

	public boolean[][] getStuffMatrix() {
		return stuffMatrix;
	}

	public void setStuffMatrix(boolean[][] cactusMatrix) {
		this.stuffMatrix = cactusMatrix;
	}

	public List<Enemy> getEnemies() {
		return enemies;
	}

	public void setEnemies(List<Enemy> enemies) {
		this.enemies = enemies;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public Class<? extends BaseVectorShape> getType() {
		return type;
	}

	public void setType(Class<? extends BaseVectorShape> type) {
		this.type = type;
	}

	public Color getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public String getSound() {
		return sound;
	}

	public void setSound(String sound) {
		this.sound = sound;
	}

}
