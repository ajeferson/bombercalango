package br.com.bombercalango.model;

import java.awt.Color;
import java.awt.Polygon;

import br.com.bombercalango.graphic.BaseVectorShape;

/**
 * Abstract a indestructible rock element.
 * @author Alan Jeferson
 * */
public class Rock extends BaseVectorShape{
	
	private int[] xCoords = {2, 4, 16, 18, 18, 16, 4, 2, 2};
	private int[] yCoords = {4, 2, 2, 4, 16, 18, 18, 16, 4};
	
	public Rock(int x, int y){
		this.setShape(new Polygon(this.xCoords, this.yCoords, this.xCoords.length));
		this.setColor(Color.LIGHT_GRAY);
		this.setX(x);
		this.setY(y);
	}

}
