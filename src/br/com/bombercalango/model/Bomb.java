package br.com.bombercalango.model;

import br.com.bombercalango.graphic.BaseVectorShape;
import br.com.bombercalango.other.ExplosionRange;

/**
 * Abstraction of a bomb.
 * @author Alan Jeferson
 * */
public class Bomb extends BaseVectorShape {

	private final int DEFAULT_ANIMATION_TIME = 60;
	private final int DEFAULT_LIFETIME = 15;
	
	private int coordX, coordY;
	private boolean increasing;
	private int radius;
	private int animationTime;
	private int lifetime;
	private ExplosionRange explosionRange;
	
	public Bomb(int coordX, int coordY, int radius, boolean[][] rockMatrix){
		this.coordX = coordX;
		this.coordY = coordY;
		this.radius = radius;
		this.animationTime = DEFAULT_ANIMATION_TIME;
		this.lifetime = DEFAULT_LIFETIME;
		this.explosionRange = new ExplosionRange(this.coordX, this.coordY, rockMatrix);
	}

	public int getCoordY() {
		return coordY;
	}

	public void setCoordY(int coordY) {
		this.coordY = coordY;
	}

	public int getCoordX() {
		return coordX;
	}

	public void setCoordX(int coordX) {
		this.coordX = coordX;
	}

	public boolean isIncreasing() {
		return increasing;
	}

	public void setIncreasing(boolean increasing) {
		this.increasing = increasing;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}
	
	public void increaseRadius(){
		this.radius++;
	}
	
	public void decreaseRadius(){
		this.radius--;
	}

	public int getLifeTime() {
		return animationTime;
	}

	public void decreaseAnimationTime(){
		this.animationTime--;
	}
	
	public void restoreAnimationTime(){
		this.animationTime = DEFAULT_ANIMATION_TIME;
		this.lifetime--;
	}

	public int getLifetime() {
		return lifetime;
	}

	public ExplosionRange getExplosionRange() {
		return explosionRange;
	}
	
}
