package br.com.bombercalango.model.enemy;

import java.awt.Color;
import java.awt.Graphics2D;

import br.com.bombercalango.other.EnemyDirection;

/**
 * Hamburguer enemy.
 * @author Alan Jeferson
 * */
public class Hamburger extends Enemy{
	
	public Hamburger(int coordX, int coordY){
		super(4);
		this.direction = EnemyDirection.UPWARD;
		this.coordX = coordX;
		this.coordY = coordY;
	}
	
	@Override
	public void drawEnemy(Graphics2D graphics) {
		graphics.setColor(new Color(240, 197, 96));
		graphics.fillOval(10, 12, 40, 35);
		graphics.setColor(new Color(74, 62, 2));
		graphics.fillRoundRect(6, 24, 48, 12, 6, 6);
	}
	
}
