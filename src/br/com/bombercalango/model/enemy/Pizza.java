package br.com.bombercalango.model.enemy;

import java.awt.Color;
import java.awt.Graphics2D;

import br.com.bombercalango.other.EnemyDirection;

public class Pizza extends Enemy{

	public Pizza(int coordX, int coordY){
		super(2);
		this.direction = EnemyDirection.UPWARD;
		this.coordX = coordX;
		this.coordY = coordY;
	}
	
	@Override
	public void drawEnemy(Graphics2D graphics) {
		graphics.setColor(new Color(240, 197, 96));
		graphics.scale(3, 3);
		graphics.fillOval(3, 2, 15, 15);
	}

}
