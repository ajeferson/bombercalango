package br.com.bombercalango.model.enemy;

import java.awt.Color;
import java.awt.Graphics2D;

import br.com.bombercalango.other.EnemyDirection;

/**
 * A coke enemy.
 * @author Alan Jeferson.
 * */
public class Coke extends Enemy {
	
	public Coke(int coordX, int coordY){
		super(3);
		this.direction = EnemyDirection.UPWARD;
		this.coordX = coordX;
		this.coordY = coordY;
	}

	@Override
	public void drawEnemy(Graphics2D graphics) {
		graphics.setColor(Color.RED);
		graphics.scale(3, 3);
		graphics.fillRoundRect(4, 2, 13, 17, 4, 4);
	}
	
}
