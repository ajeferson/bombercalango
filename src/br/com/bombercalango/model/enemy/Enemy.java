package br.com.bombercalango.model.enemy;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import br.com.bombercalango.model.Bomb;
import br.com.bombercalango.model.Explosion;
import br.com.bombercalango.other.EnemyDirection;
import br.com.bombercalango.other.ExplosionRange;

/**
 * General enemy.
 * @author Alan Jeferson.
 * */
public abstract class Enemy{

	protected int coordX, coordY;
	protected EnemyDirection direction;
	private int incrementX, incrementY;
	private int defaultAnimationTime = 5;
	private int animationTime;

	public Enemy(int animationTime){
		defaultAnimationTime = animationTime;
		this.animationTime = defaultAnimationTime;
	}

	/**
	 * Draws the enemy on the screen.
	 * */
	public abstract void drawEnemy(Graphics2D graphics);

	/**
	 * Change enemy position.
	 * */
	public void move(boolean[][] rockMatrix, boolean[][] cactusMatrix, Bomb bomb, Explosion explosion){
		if((((this.direction.equals(EnemyDirection.UPWARD) || this.direction.equals(EnemyDirection.DOWNWARD)) && this.incrementX==0)
				|| ((this.direction.equals(EnemyDirection.LEFTWARD) || this.direction.equals(EnemyDirection.RIGHTWARD)) && this.incrementY==0))
				|| this.direction.equals(EnemyDirection.NONE)){
			if(!this.canMove(rockMatrix, cactusMatrix, bomb, explosion)){
				List<EnemyDirection> directions = new ArrayList<EnemyDirection>();

				for(int i=0;i<EnemyDirection.values().length;i++){
					if(!EnemyDirection.values()[i].equals(this.direction) &&
							!EnemyDirection.values()[i].equals(EnemyDirection.NONE)){
						directions.add(EnemyDirection.values()[i]);
					}
				}

				Random random = new Random();
				int index = 0;
				while(!directions.isEmpty()){
					index = random.nextInt(directions.size());
					this.direction = directions.get(index);
					if(this.canMove(rockMatrix, cactusMatrix, bomb, explosion)){
						break;
					} else{
						directions.remove(index);
					}
				}
				if(directions.isEmpty()) this.direction = EnemyDirection.NONE;
			}
		}

		if(!this.direction.equals(EnemyDirection.NONE)){
			switch (this.direction) {
			case UPWARD:
				if(this.incrementX>-60) this.incrementX-=1;
				else{
					this.coordX--;
					this.incrementX = 0;
				}
				break;
			case DOWNWARD:
				if(this.incrementX<60) this.incrementX+=1;
				else{
					this.coordX++;
					this.incrementX = 0;
				}
				break;
			case LEFTWARD:
				if(this.incrementY>-60) this.incrementY-=1;
				else{
					this.coordY--;
					this.incrementY = 0;
				}
				break;
			case RIGHTWARD:
				if(this.incrementY<60) this.incrementY+=1;
				else{
					this.coordY++;
					this.incrementY = 0;
				}
				break;
			default:
				break;
			}
		}

	}

	private boolean canMove(boolean[][] rockMatrix, boolean[][] cactusMatrix, Bomb bomb, Explosion explosion){
		ExplosionRange range = null;
		if(bomb!=null){
			range = bomb.getExplosionRange();
		} else if(explosion!=null){
			range = explosion.getExplosionRange();
		}
		switch (this.direction) {
		case UPWARD:
			if(this.coordX-1>=0 && 
			!rockMatrix[coordX-1][coordY] && 
			!cactusMatrix[coordX-1][coordY]){
				if(range!=null && range.isInExplosionRange(coordX-1, coordY)){
					if(this.coordX-Explosion.EXPLOSION_POWER>=0 && 
							!rockMatrix[coordX-Explosion.EXPLOSION_POWER][coordY] && 
							!cactusMatrix[coordX-Explosion.EXPLOSION_POWER][coordY]){
						return !(range.isInExplosionRange(coordX-Explosion.EXPLOSION_POWER, coordY)) &&
								range.getOriginX()==coordY;
					}
					return false;
				}
				return true;
			} 
			return false;
			/*(bomb==null || bomb.getCoordY()>coordX || 
				coordX-1>bomb.getCoordY()+Explosion.EXPLOSION_POWER || coordY!=bomb.getCoordX());*/
		case DOWNWARD:
			if(this.coordX+1<11 && 
					!rockMatrix[coordX+1][coordY] && 
					!cactusMatrix[coordX+1][coordY]){
				if(range!=null && range.isInExplosionRange(coordX+1, coordY)){
					if(this.coordX+Explosion.EXPLOSION_POWER<11 && 
							!rockMatrix[coordX+Explosion.EXPLOSION_POWER][coordY] && 
							!cactusMatrix[coordX+Explosion.EXPLOSION_POWER][coordY]){
						return !(range.isInExplosionRange(coordX+Explosion.EXPLOSION_POWER, coordY)) &&
								range.getOriginX()==coordY;
					}
					return false;
				}
				return true;
			}
			return false;
					/*(bomb==null || coordX+1<bomb.getCoordY()-Explosion.EXPLOSION_POWER || 
						coordX>bomb.getCoordY() || coordY!=bomb.getCoordX());*/
		case LEFTWARD:
			if(this.coordY-1>=0 && 
	  		!rockMatrix[coordX][coordY-1] && 
			!cactusMatrix[coordX][coordY-1]){
				if(range!=null && range.isInExplosionRange(coordX, coordY-1)){
					if(this.coordY-Explosion.EXPLOSION_POWER>=0 && 
					  		!rockMatrix[coordX][coordY-Explosion.EXPLOSION_POWER] && 
							!cactusMatrix[coordX][coordY-Explosion.EXPLOSION_POWER]){
						return !(range.isInExplosionRange(coordX, coordY-Explosion.EXPLOSION_POWER)) &&
								range.getOriginY()==coordX;
					}
					return false;
				}
				return true;
			}
			return false;
			/*(bomb==null || coordY-1>bomb.getCoordX()+Explosion.EXPLOSION_POWER ||
					coordY<bomb.getCoordX() || coordX!=bomb.getCoordY());*/
		case RIGHTWARD:
			if(this.coordY+1<11 && 
					!rockMatrix[coordX][coordY+1] && 
					!cactusMatrix[coordX][coordY+1]){
				if(range!=null && range.isInExplosionRange(coordX, coordY+1)){
					if(this.coordY+Explosion.EXPLOSION_POWER<11 && 
							!rockMatrix[coordX][coordY+Explosion.EXPLOSION_POWER] && 
							!cactusMatrix[coordX][coordY+Explosion.EXPLOSION_POWER]){
						return !(range.isInExplosionRange(coordX, coordY+Explosion.EXPLOSION_POWER)) &&
								range.getOriginY()==coordX;
					}
					return false;
				}
				return true;
			}
			return false;
					/*(bomb==null || coordY+1<bomb.getCoordX()-Explosion.EXPLOSION_POWER ||
						coordY>bomb.getCoordX() || coordX!=bomb.getCoordY());*/
		default:
			return false;
		}
	}

	public int getCoordX() {
		return coordX;
	}

	public void setCoordX(int coordX) {
		this.coordX = coordX;
	}

	public int getCoordY() {
		return coordY;
	}

	public void setCoordY(int coordY) {
		this.coordY = coordY;
	}

	public EnemyDirection getDirection() {
		return direction;
	}

	public void setDirection(EnemyDirection direction) {
		this.direction = direction;
	}

	public int getTranslationY() {
		return this.getCoordY()*60+this.incrementY;
	}

	public int getTranslationX() {
		return this.getCoordX()*60+this.incrementX;
	}

	public int getAnimationTime() {
		return animationTime;
	}

	public void decreaseAnimationTime() {
		this.animationTime--;
	}

	public void restoreAnimationTime(){
		this.animationTime = defaultAnimationTime;
	}

}
