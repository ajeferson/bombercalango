package br.com.bombercalango.model;

import br.com.bombercalango.graphic.BaseVectorShape;
import br.com.bombercalango.other.ExplosionRange;

/**
 * Abstraction of an explosion of a bomb.
 * @author Alan Jeferson
 * */
public class Explosion extends BaseVectorShape{

	private final int DEFAULT_LIFETIME = 250;
	public static final int EXPLOSION_POWER = 2;

	private int xBegin, xEnd;
	private int yBegin, yEnd;
	private int lifetime;
	private ExplosionRange explosionRange;

	private int xCenter, yCenter;

	public Explosion(Bomb bomb, boolean[][] rockMatrix){
		this.lifetime = DEFAULT_LIFETIME;
		this.setExplosionRange(new ExplosionRange(bomb.getCoordX(), bomb.getCoordY(), rockMatrix));

		/* Getting X coordenates */
		this.xBegin = bomb.getCoordX()-EXPLOSION_POWER;
		this.xEnd = bomb.getCoordX()+EXPLOSION_POWER+1;
		for(int i=bomb.getCoordX();i>=xBegin && i>=0;i--){ //Left limit
			if(rockMatrix[bomb.getCoordY()][i]){
				this.xBegin = i+1;
				break;
			}
		}
		for(int i=bomb.getCoordX()>0 ? bomb.getCoordX() : 0;
				i<=xEnd && i<11;i++){ //Right limit
			if(rockMatrix[bomb.getCoordY()][i]){
				this.xEnd = i-2;
				break;
			}
		}
		this.yCenter = bomb.getCoordY();

		/* Getting Y coordenates */
		this.yBegin = bomb.getCoordY()-EXPLOSION_POWER;
		this.yEnd = bomb.getCoordY()+EXPLOSION_POWER+1;
		for(int i=bomb.getCoordY();i<yEnd && i<11;i++){ //Inferior limit
			if(rockMatrix[i][bomb.getCoordX()]){
				this.yEnd = i;
				break;
			}
		}
		for(int i=bomb.getCoordY();i>=yBegin && i>=0;i--){ //Superior Limit
			if(rockMatrix[i][bomb.getCoordX()]){
				this.yBegin = i+2;
				break;
			}
		}
		this.xCenter = bomb.getCoordX();
	}

	public int getLifetime() {
		return lifetime;
	}

	public void decreaseLifetime(){
		this.lifetime--;
	}

	public int getxBegin() {
		return xBegin;
	}

	public int getxEnd() {
		return xEnd;
	}

	public int getYCenter() {
		return yCenter;
	}

	public int getXCenter() {
		return xCenter;
	}

	public int getyBegin() {
		return yBegin;
	}

	public int getyEnd() {
		return yEnd;
	}

	public ExplosionRange getExplosionRange() {
		return explosionRange;
	}

	public void setExplosionRange(ExplosionRange explosionRange) {
		this.explosionRange = explosionRange;
	}

}
