package br.com.bombercalango.model;

import java.awt.Polygon;

import br.com.bombercalango.graphic.BaseVectorShape;

/**
 * Abstracts a calango player
 * @author Alan Jeferson
 * */
public class Calango extends BaseVectorShape{
	
	public static final int DEFAULT_LIVES = 5;
	
	private int[] xCoords = {3, 0, 0, 3, 5, 5, 4, 4, 6, 8, 8, 7, 7, 9, 12, 12, 9, 7, 7, 
								9, 12, 12, 9, 7, 7, 5, 2, 4, 5, 5, 3, 0, 0, 3, 5, 5, 3};
	private int[] yCoords = {9, 7, 5, 7, 7, 6, 5, 2, 0, 2, 5, 6, 7, 7, 5, 7, 9, 9, 12, 12, 
							10, 12, 14, 14, 17, 20, 20, 19, 17, 14, 14, 12, 10, 12, 12, 9, 9};
	private int lives;
	
	private int kills;
	
	private boolean dead;
	
	private int coordX, coordY;
	
	public Calango(){
		this.setShape(new Polygon(this.xCoords, this.yCoords, this.xCoords.length));
		this.setLives(DEFAULT_LIVES);
		this.setFaceAngle(90);
		this.coordX = 0;
		this.coordY = 0;
		this.kills = 0;
	}

	public int getLives() {
		return lives;
	}

	public void setLives(int lives) {
		this.lives = lives;
	}

	public int getCoordX() {
		return coordX;
	}

	public void setCoordX(int coordX) {
		this.coordX = coordX;
	}

	public int getCoordY() {
		return coordY;
	}

	public void setCoordY(int coordY) {
		this.coordY = coordY;
	}

	public int getKills() {
		return kills;
	}
	
	public void restoreKills(){
		this.kills = 0;
	}

	public void incrementKills(){
		this.kills++;
	}
	
	public void die(){
		this.lives--;
		this.dead = true;
		this.restorePosition();
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}
	
	public void restorePosition(){
		this.coordX = 0;
		this.coordY = 0;
		this.setFaceAngle(90);
	}

}
